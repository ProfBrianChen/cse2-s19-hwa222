//Henry Anderson Lab 02 for CSE2 on 2/1/2019
//The following program will print the number of minutes, counts, and distances for either one or two bicycle trips (return trip) 

//creates class essential for start of program
public class Cyclometer {
  //main method required for every Java program
  public static void main(String[] args) {
    //our input data
    int secsTrip1=480;  //input for duration of trip 1 (in seconds)
    int secsTrip2=3220;  //input for duration of trip 2 (in seconds)
		int countsTrip1=1561;  //input for number of counts in trip 1
		int countsTrip2=9037; //input for number of counts in trip 2
    
    //our intermediate variables output data
    double wheelDiameter=27.0,  //value for diameter of wheel, 
  	PI=3.14159, //constant pi, used for circular calculations (with the wheelDiameter)
  	feetPerMile=5280,  //constant for number of feet in a mile
  	inchesPerFoot=12,   //constant for number of inches in a foot
  	secondsPerMinute=60;  //constant for number of seconds in a minute
	  double distanceTrip1, distanceTrip2,totalDistance;  //creates variables for distance of trips 1 and 2, and for total distanceTrip
    
    //priniting numbers stored in variables using conversion from seconds to minutes, displaying counts as well
    System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
    
    //runs the calculations; stores the values. we are calculating the distance for the trips using stored variables
       distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	    totalDistance=distanceTrip1+distanceTrip2;
    //Print out the output data.
           System.out.println("Trip 1 was "+distanceTrip1+" miles");
	         System.out.println("Trip 2 was "+distanceTrip2+" miles");
	         System.out.println("The total distance was "+totalDistance+" miles");
	
    
  } //end of the main method 
} //end of class
