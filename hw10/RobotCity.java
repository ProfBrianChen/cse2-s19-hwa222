//Henry Anderson HW10 for CSE2 on 4/30/19
//The following program utilizes arrays

//imports classes
import java.util.Arrays;
import java.util.Random;
//creates essential class for the start of every program
public class RobotCity {            
  //creates essential main method
  public static void main(String[] args) {
    Random r = new Random();
    int [] [] cityArray = buildCity();
    System.out.println("City Built");
    display(cityArray);
    int k =r.nextInt(50);
    k +=1;
    invade(cityArray,k);
    System.out.println("City Invaded");
    display(cityArray);
    for(int i=0; i<5; i++){
    update(cityArray);
    System.out.println("City Updated");
    display(cityArray);
    }
     
  }
    
    public static int [] []buildCity() {
      Random r = new Random();
      int m = r.nextInt(6);
      m +=10;
      int n = r.nextInt(6);
      n =+10;
      int [] []cityArray = new int [m][n];
    
    
    for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++) {
        cityArray[i][j] = (r.nextInt(900)+100);
                 }
              } return cityArray;
          }
    
     public static void display(int[][] cityArray) {
         for(int i = 0; i < cityArray.length; i++){
            for(int j = 0; j < cityArray[0].length; j++){
             System.out.printf("% 4d",cityArray[i][j]);
                    }
              System.out.println();
                }
         }
    public static int [] []invade(int [][]cityArray, int k) {
      Random r = new Random();  
      for(int i =0; i<k; i++){
          int m= r.nextInt(cityArray.length);
          int n= r.nextInt(cityArray[0].length);
          if (cityArray[m][n]>0){
          cityArray[m][n] = (-1)*cityArray [m][n];
          }
          else {
            k =+1;
          } 
        } return cityArray;
    }
     public static int [] []update(int [][]cityArray) { 
       for(int i = 0; i < cityArray.length; i++){
            for(int j = cityArray[0].length-1; j>=0; j--){
              if(cityArray[i][j]<0){
                cityArray[i][j] *=(-1);
               if(j!=cityArray[0].length-1){
                cityArray[i][j+1] *=(-1);
              }
              }
             } 
            }return cityArray;
           }
      
      
}
      
    