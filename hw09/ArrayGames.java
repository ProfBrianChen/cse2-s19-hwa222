//Henry Anderson HW09 for CSE2 on 4/5/19
//The following program utilizes arrays

//imports scanner class
import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;
//creates essential class for the start of every program
public class ArrayGames
{
  //creates essential main method
  
  //main method that generates two potential arrays
  //asks user which method they want to use
  //Prints out results
  public static void main(String[] args) {
   int [] array1 = generate();
   int [] array2 = generate();
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Would you like to run Insert or Shorten?");
    String preference = myScanner.nextLine();
    if(preference.equals("Insert")){
      System.out.print("Input 1: ");
      print(array1);
      System.out.print("Input 2: ");
      print(array2);
      int [] newArray = insert(array1, array2);
      System.out.print("Output: ");
      print(newArray);
    }
    
    else if (preference.equals("Shorten")){
      System.out.print("Enter Integer Input Value: ");
      int InputVal = myScanner.nextInt();
      System.out.print("Input 1: ");
      print(array1);
      System.out.println("Input 2: " + InputVal);
      int [] newArray = shorten(array1, InputVal);
      System.out.print("Output: ");
      print(newArray);
    }
    
   
  }
   //method for randomly assigning values to the arrays
    public static int [] generate() {   
      Random r = new Random();
      int [] array1 = new int[r.nextInt((10) + 1) + 10];
      for(int i = 0; i < array1.length; i++){
        array1[i] = r.nextInt(10);
      }
      return array1;                        
                              
    }
    public static void print(int[] array)
     {
         for(int i = 0; i < array.length; i++){
           System.out.print(array[i] + " ");
         }
      System.out.println();
       

     }
  //method for insert option
     public static int[] insert(int [] array1, int [] array2){
       int [] newArray = new int[array1.length + array2.length];
       Random r = new Random();
       int rand = r.nextInt(array1.length);
       int count = 0;
       int count2 = 0;
       for(int i = 0; i < newArray.length; i++)
       {
         if(count < rand){
            newArray[i] = array1[count]; 
           count++;
         } 
         else if(count2 < array2.length){
           newArray[i] = array2[count2];
           count2++;
         }
         else{
           newArray[i] = array1[count];
           count++;
         }
         
       } return newArray;
     } 
  //method for shorten option
    public static int[] shorten(int [] array1, int InputVal){
       int [] newArray = new int[array1.length-1];
     
       for(int i = 0; i < (array1.length-1); i++)
       {
        if(i==InputVal){
          
        }
         else{
           newArray[i]=array1[i];
         }
         
       } return newArray;
     }
 }

