//Henry Anderson HW #2 For CSE2 on 2/8/19
//The following program takes given values for prices and calculates the final cost with sales tax

//main method required for any sequence of code
public class Arithmetic {
//class required for any sequence of code
  public static void main(String[] args) {
  
//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltPrice = 33.99;

//the tax rate
double paSalesTax = 0.06;
        
//total cost of each item
double totalcostofpants;
double totalcostofshirts;
double totalcostofbelts;
        
//sales tax on each item
double justtaxpants;
double justtaxshirts;
double justtaxbelts;
        
//total sales tax and untaxed total price   
double totaltax;
double untaxedtotal;
        
//overall total
double overalltotal;
       
//calculates costs  
totalcostofpants=((numPants)*(pantsPrice));
totalcostofshirts=((numShirts)*(shirtPrice));
totalcostofbelts=((numBelts)*(beltPrice));    
  
//calculates sales tax on each item
justtaxpants=totalcostofpants*paSalesTax*100;
justtaxshirts=totalcostofshirts*paSalesTax*100;
justtaxbelts=totalcostofbelts*paSalesTax*100;
    
//multiplies tax values by 100 
//double tp= justtaxpants;
//double ts= justtaxshirts;
//double tb= justtaxbelts;
    
//casts sales tax values to an integer. this will allow for them to be printed with only 2 decimal places
int tpint100= (int) (justtaxpants);
int tsint100= (int) (justtaxshirts);
int tbint100= (int) (justtaxbelts);
    
//diviides integer values by 100, allowing two decimal value to be stored
double tpdec= (double) (tpint100);
double tpint= (tpdec/100);
    
double tsdec= (double) (tsint100);
double tsint= (tsdec/100);
    
double tbdec= (double) (tbint100);
double tbint= (tbdec/100);
  
//calculates total price before tax
untaxedtotal=(totalcostofbelts+totalcostofshirts+totalcostofpants);
  
//calculates total tax
totaltax=(tpint+tsint+tbint);
  
//calculates overall total
overalltotal=totaltax+untaxedtotal;
  
//prints untaxed total costs for each item
System.out.println("The total cost of the pants before tax was $"+(totalcostofpants)+"");
System.out.println("The total cost of the shirts before tax was $"+(totalcostofshirts)+"");  
System.out.println("The total cost of the belts before tax was $"+(totalcostofbelts)+"");  

//prints only taxes for each item
System.out.println("The total tax on the pants was $"+(tpint)+"");
System.out.println("The total tax on the shirts was $"+(tsint)+"");  
System.out.println("The total tax on the belts was $"+(tbint)+""); 
  
//prints total price before tax
System.out.println("The total price before tax was $"+(untaxedtotal)+"");  
 
//prints tax total  
System.out.println("The total tax was $"+(totaltax)+"");
  
//prints overall total
System.out.println("The overall total was $"+(overalltotal)+"");

  }
}