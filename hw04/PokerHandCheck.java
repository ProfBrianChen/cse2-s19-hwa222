//Henry Anderson HW 04 for CSE2 on 2/15/2019
//The following program will simulate drawing a "random" card from three different decks, and determine if the player has a pair, two pair, or three of a kind


public class PokerHandCheck {            //creates essential class for the start of every program
  
  public static void main(String[] args) {             //creates essential main method for the start of every program
        
//generates 5 random numbers for the five cards in the deck    
int rand1 = (int)(Math.random() * ((52) + 1)); //generates random number between 1-52, representing the number of cards in the deck
int rand2 = (int)(Math.random() * ((52) + 1)); //generates random number between 1-52, representing the number of cards in the deck
int rand3 = (int)(Math.random() * ((52) + 1)); //generates random number between 1-52, representing the number of cards in the deck    
int rand4 = (int)(Math.random() * ((52) + 1)); //generates random number between 1-52, representing the number of cards in the deck
int rand5 = (int)(Math.random() * ((52) + 1)); //generates random number between 1-52, representing the number of cards in the deck

//decides number for each of the five cards    
int num1 = (rand1%13); //uses modulus command to calculate for number card (ace, king, 3, etc)
int num2 = (rand2%13); //uses modulus command to calculate for number card (ace, king, 3, etc)
int num3 = (rand3%13); //uses modulus command to calculate for number card (ace, king, 3, etc)
int num4 = (rand4%13); //uses modulus command to calculate for number card (ace, king, 3, etc)
int num5 = (rand5%13); //uses modulus command to calculate for number card (ace, king, 3, etc)
 
//decides suit for each of the five cards   
int suit1 = (rand1%4); //uses modulus to calculate for suit
int suit2 = (rand2%4); //uses modulus to calculate for suit
int suit3 = (rand3%4); //uses modulus to calculate for suit
int suit4 = (rand4%4); //uses modulus to calculate for suit
int suit5 = (rand5%4); //uses modulus to calculate for suit
    
//variables for storing overall number values    
int ace=0;
int two=0;
int three=0;
int four=0;
int five=0;
int six=0;
int seven=0;
int eight=0;    
int nine=0;
int ten=0;
int jack=0;
int queen=0;
int king=0;    
    
    
System.out.println("Your Random Cards Were:"); //prints opening statment before carda are generated
    
//switch statement determines and prints the number of card  
switch( num1 ) {
  case 0:
    System.out.print("Ace of");   //prints number value(Ace, King 3, etc.)
    ace= ace+1;
    break;
    
  case 1:
    System.out.print("2 of"); //prints number value(Ace, King 3, etc.)
    two=two+1;
    break;
    
  case 2:
    System.out.print("3 of");   //prints number value(Ace, King 3, etc.)
    three=three+1;
    break;
    
  case 3:
    System.out.print("4 of");   //prints number value(Ace, King 3, etc.)
    four=four+1;
    break;
    
  case 4:
    System.out.print("5 of");   //prints number value(Ace, King 3, etc.)
    five=five+1;
    break;
    
  case 5:
    System.out.print("6 of");   //prints number value(Ace, King 3, etc.)
    six=six+1;
    break;
    
  case 6:
    System.out.print("7 of");   //prints number value(Ace, King 3, etc.)
    seven=seven+1;
    break;
    
  case 7:
    System.out.print("8 of");   //prints number value(Ace, King 3, etc.)
    eight=eight+1;
    break;  
    
  case 8:
    System.out.print("9 of");   //prints number value(Ace, King 3, etc.)
    nine=nine+1;
    break;
    
  case 9:
    System.out.print("10 of");   //prints number value(Ace, King 3, etc.)
    ten=ten+1;
    break;
    
  case 10:
    System.out.print("Jack of");   //prints number value(Ace, King 3, etc.)
    jack=jack+1;
    break;  
    
  case 11:
    System.out.print("Queen of");   //prints number value(Ace, King 3, etc.)
    queen=queen+1;
    break;
    
  case 12:
    System.out.print("King of");   //prints number value(Ace, King 3, etc.)
    king=king+1;
    break;  
}
    
//switch statment determines and prints the suit of card
switch(suit1){
  case 0:
     System.out.print(" Spades ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
    
  case 1:
     System.out.print(" Hearts ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
   
   case 2:
     System.out.print(" Clubs ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
    
    
   case 3:
     System.out.print(" Diamonds ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
}
  
    System.out.println(""); //blank for spacing 
    
    //repeats for next card
    
    //switch statement determines and prints the number of card  
switch( num2 ) {
  case 0:
    System.out.print("Ace of");//prints number value(Ace, King 3, etc.)
    ace= ace+1;
    break;
    
  case 1:
    System.out.print("2 of");   //prints number value(Ace, King 3, etc.)
    two=two+1;
    break;
    
  case 2:
    System.out.print("3 of");   //prints number value(Ace, King 3, etc.)
    three=three+1;
    break;
    
  case 3:
    System.out.print("4 of");   //prints number value(Ace, King 3, etc.)
    four=four+1;
    break;
    
  case 4:
    System.out.print("5 of");   //prints number value(Ace, King 3, etc.)
    five=five+1;
    break;
    
  case 5:
    System.out.print("6 of");   //prints number value(Ace, King 3, etc.)
    six=six+1;
    break;
    
  case 6:
    System.out.print("7 of");   //prints number value(Ace, King 3, etc.)
    seven=seven+1;
    break;
    
  case 7:
    System.out.print("8 of");   //prints number value(Ace, King 3, etc.)
    eight=eight+1;
    break;  
    
  case 8:
    System.out.print("9 of");   //prints number value(Ace, King 3, etc.)
    nine=nine+1;
    break;
    
  case 9:
    System.out.print("10 of");   //prints number value(Ace, King 3, etc.)
    ten=ten+1;
    break;
    
  case 10:
    System.out.print("Jack of");   //prints number value(Ace, King 3, etc.)
    jack=jack+1;
    break;  
    
  case 11:
    System.out.print("Queen of");   //prints number value(Ace, King 3, etc.)
    queen=queen+1;
    break;
    
  case 12:
    System.out.print("King of");   //prints number value(Ace, King 3, etc.)
    king=king+1;
    break;  
}
    
//switch statment determines and prints the suit of card
switch(suit2){
  case 0:
     System.out.print(" Spades ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
    
  case 1:
     System.out.print(" Hearts ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
   
   case 2:
     System.out.print(" Clubs ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
    
    
   case 3:
     System.out.print(" Diamonds ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
}
    System.out.println(""); //blank for spacing
    
    
    //repeats for next card
    
    //switch statement determines and prints the number of card  
switch( num3 ) {
  case 0:
    System.out.print("Ace of");   //prints number value(Ace, King 3, etc.)
    ace= ace+1;
    break;
    
  case 1:
    System.out.print("2 of");   //prints number value(Ace, King 3, etc.)
    two=two+1;
    break;
    
  case 2:
    System.out.print("3 of");   //prints number value(Ace, King 3, etc.)
    three=three+1;
    break;
    
  case 3:
    System.out.print("4 of");   //prints number value(Ace, King 3, etc.)
    four=four+1;
    break;
    
  case 4:
    System.out.print("5 of");   //prints number value(Ace, King 3, etc.)
    five=five+1;
    break;
    
  case 5:
    System.out.print("6 of");   //prints number value(Ace, King 3, etc.)
    six=six+1;
    break;
    
  case 6:
    System.out.print("7 of");   //prints number value(Ace, King 3, etc.)
    seven=seven+1;
    break;
    
  case 7:
    System.out.print("8 of");   //prints number value(Ace, King 3, etc.)
    eight=eight+1;
    break;  
    
  case 8:
    System.out.print("9 of");   //prints number value(Ace, King 3, etc.)
    nine=nine+1;
    break;
    
  case 9:
    System.out.print("10 of");   //prints number value(Ace, King 3, etc.)
    ten=ten+1;
    break;
    
  case 10:
    System.out.print("Jack of");   //prints number value(Ace, King 3, etc.)
    jack=jack+1;
    break;  
    
  case 11:
    System.out.print("Queen of");   //prints number value(Ace, King 3, etc.)
    queen=queen+1;
    break;
    
  case 12:
    System.out.print("King of");   //prints number value(Ace, King 3, etc.)
    king=king+1;
    break;  
}
    
//switch statment determines and prints the suit of card
switch(suit3){
  case 0:
     System.out.print(" Spades ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
    
  case 1:
     System.out.print(" Hearts ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
   
   case 2:
     System.out.print(" Clubs ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
    
    
   case 3:
     System.out.print(" Diamonds ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
}
    
    System.out.println(""); //blank for spacing
    
    //repeats for next card
    
    //switch statement determines and prints the number of card  
switch( num4 ) {
  case 0:
    System.out.print("Ace of");   //prints number value(Ace, King 3, etc.)
    ace= ace+1;
    break;
    
  case 1:
    System.out.print("2 of");   //prints number value(Ace, King 3, etc.)
    two=two+1;
    break;
    
  case 2:
    System.out.print("3 of");   //prints number value(Ace, King 3, etc.)
    three=three+1;
    break;
    
  case 3:
    System.out.print("4 of");   //prints number value(Ace, King 3, etc.)
    four=four+1;
    break;
    
  case 4:
    System.out.print("5 of");   //prints number value(Ace, King 3, etc.)
    five=five+1;
    break;
    
  case 5:
    System.out.print("6 of");   //prints number value(Ace, King 3, etc.)
    six=six+1;
    break;
    
  case 6:
    System.out.print("7 of");   //prints number value(Ace, King 3, etc.)
    seven=seven+1;
    break;
    
  case 7:
    System.out.print("8 of");   //prints number value(Ace, King 3, etc.)
    eight=eight+1;
    break;  
    
  case 8:
    System.out.print("9 of");   //prints number value(Ace, King 3, etc.)
    nine=nine+1;
    break;
    
  case 9:
    System.out.print("10 of");   //prints number value(Ace, King 3, etc.)
    ten=ten+1;
    break;
    
  case 10:
    System.out.print("Jack of");   //prints number value(Ace, King 3, etc.)
    jack=jack+1;
    break;  
    
  case 11:
    System.out.print("Queen of");   //prints number value(Ace, King 3, etc.)
    queen=queen+1;
    break;
    
  case 12:
    System.out.print("King of");   //prints number value(Ace, King 3, etc.)
    king=king+1;
    break;  
}
    
//switch statment determines and prints the suit of card
switch(suit4){
  case 0:
     System.out.print(" Spades ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
    
  case 1:
     System.out.print(" Hearts ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
   
   case 2:
     System.out.print(" Clubs ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
    
    
   case 3:
     System.out.print(" Diamonds ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
}
    
    System.out.println(""); //blank for spacing
    
    //repeats for next card
    
    //switch statement determines and prints the number of card  
switch( num5 ) {
  case 0:
    System.out.print("Ace of");   //prints number value(Ace, King 3, etc.)
    ace= ace+1;
    break;
    
  case 1:
    System.out.print("2 of");   //prints number value(Ace, King 3, etc.)
    two=two+1;
    break;
    
  case 2:
    System.out.print("3 of");   //prints number value(Ace, King 3, etc.)
    three=three+1;
    break;
    
  case 3:
    System.out.print("4 of");   //prints number value(Ace, King 3, etc.)
    four=four+1;
    break;
    
  case 4:
    System.out.print("5 of");   //prints number value(Ace, King 3, etc.)
    five=five+1;
    break;
    
  case 5:
    System.out.print("6 of");   //prints number value(Ace, King 3, etc.)
    six=six+1;
    break;
    
  case 6:
    System.out.print("7 of");   //prints number value(Ace, King 3, etc.)
    seven=seven+1;
    break;
    
  case 7:
    System.out.print("8 of");   //prints number value(Ace, King 3, etc.)
    eight=eight+1;
    break;  
    
  case 8:
    System.out.print("9 of");   //prints number value(Ace, King 3, etc.)
    nine=nine+1;
    break;
    
  case 9:
    System.out.print("10 of");   //prints number value(Ace, King 3, etc.)
    ten=ten+1;
    break;
    
  case 10:
    System.out.print("Jack of");   //prints number value(Ace, King 3, etc.)
    jack=jack+1;
    break;  
    
  case 11:
    System.out.print("Queen of");   //prints number value(Ace, King 3, etc.)
    queen=queen+1;
    break;
    
  case 12:
    System.out.print("King of");   //prints number value(Ace, King 3, etc.)
    king=king+1;
    break;  
}
    
//switch statment determines and prints the suit of card
switch(suit5){
  case 0:
     System.out.print(" Spades ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
    
  case 1:
     System.out.print(" Hearts ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
   
   case 2:
     System.out.print(" Clubs ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
    
    
   case 3:
     System.out.print(" Diamonds ") ;   //printing out suit(spades,hearts,kings,clubs)
    break;
}
    
    //used to test program was counting card types properly, not neccesary for final output
    
    //System.out.println(""+(ace)+"");
    //System.out.println(""+(two)+"");
    //System.out.println(""+(three)+"");
    //System.out.println(""+(four)+"");
    //System.out.println(""+(five)+"");
    //System.out.println(""+(six)+"");
    //System.out.println(""+(seven)+"");
    //System.out.println(""+(eight)+"");
    //System.out.println(""+(nine)+"");
    //System.out.println(""+(ten)+"");
    //System.out.println(""+(jack)+"");
    //System.out.println(""+(queen)+"");
    //System.out.println(""+(king)+"");
    
    
    System.out.println("");
    System.out.println(""); //blanks for spacing

    
    
//now that cards have been drawn and counted, program tests for pairs, two pair, or three of a kind    
//each time the program runs through a switch statement to test for two pairs, one less check has to be performed in the next switch statement
//the now redundant checks are placed into a else if loop that intentionally does nothing
//this prevents the "Two Pair!" statement from printing twice, as well as the "Two Pair!" statement from printing alongside the "You have a pair!"
    

switch(ace){ //if there are two or three of a number-card detected, the program proceeds to determine whether it is a pair, two pair, or three of a kind
  
  case 2: //if there are two cards, it first tests to see if there is a pair of any other cards
    
      if((two==2) || ((three)==2) || ((four)==2) || ((five)==2) || ((six)==2) || ((seven)==2) || ((eight)==2) || ((nine)==2) || ((ten)==2) || ((jack)==2) || ((queen)==2) || ((king)==2)){
      System.out.println("Two Pair!"); //if there is another pair, the program prints two pair
       }
    
       else  {
       System.out.println("You have a pair!"); //if no other pair is detected, the program prints that the player has a single pair
       }
break;
    
  
  case 3: //if there are three of one number-card, the program prints that there are three of that kind
    
    System.out.println("Three of a Kind!");
break;
  } //ends the switch statement    
    

    
    
    
switch(two){ //if there are two or three of a number-card detected, the program proceeds to determine whether it is a pair, two pair, or three of a kind
  
  case 2: //if there are two cards, it first tests to see if there is a pair of any other cards
    
      if(((three)==2) || ((four)==2) || ((five)==2) || ((six)==2) || ((seven)==2) || ((eight)==2) || ((nine)==2) || ((ten)==2) || ((jack)==2) || ((queen)==2) || ((king)==2)){
      System.out.println("Two Pair!"); //if there is another pair, the program prints two pair
       }
 
       else if (((ace)==2)){}
       else  {
       System.out.println("You have a pair!"); //if no other pair is detected, the program prints that the player has a single pair
       }
break;
    
  
  case 3: //if there are three of one number-card, the program prints that there are three of that kind
    
    System.out.println("Three of a Kind!");
break;
  } //ends the switch statement    
    
    
    
    
    
switch(three){ //if there are two or three of a number-card detected, the program proceeds to determine whether it is a pair, two pair, or three of a kind
  
  case 2: //if there are two cards, it first tests to see if there is a pair of any other cards
    
      if(((four)==2) || ((five)==2) || ((six)==2) || ((seven)==2) || ((eight)==2) || ((nine)==2) || ((ten)==2) || ((jack)==2) || ((queen)==2) || ((king)==2)){
      System.out.println("Two Pair!"); //if there is another pair, the program prints two pair
       }
    
       else if ((ace==2) || ((two)==2)){}   
      
      else  {
       System.out.println("You have a pair!"); //if no other pair is detected, the program prints that the player has a single pair
       }
break;
    
  
  case 3: //if there are three of one number-card, the program prints that there are three of that kind
    
    System.out.println("Three of a Kind!");
break;
  } //ends the switch statement    
   

    
    
    
    
switch(four){ //if there are two or three of a number-card detected, the program proceeds to determine whether it is a pair, two pair, or three of a kind
  
  case 2: //if there are two cards, it first tests to see if there is a pair of any other cards
    
      if(((five)==2) || ((six)==2) || ((seven)==2) || ((eight)==2) || ((nine)==2) || ((ten)==2) || ((jack)==2) || ((queen)==2) || ((king)==2)){
      System.out.println("Two Pair!"); //if there is another pair, the program prints two pair
       }
    
      else if ((two==2) || ((three)==2) || ((ace)==2)){}
       else  {
       System.out.println("You have a pair!"); //if no other pair is detected, the program prints that the player has a single pair
       }
break;
    
  
  case 3: //if there are three of one number-card, the program prints that there are three of that kind
    
    System.out.println("Three of a Kind!");
break;
  } //ends the switch statement    
    
    
 
    
switch(five){ //if there are two or three of a number-card detected, the program proceeds to determine whether it is a pair, two pair, or three of a kind
  
  case 2: //if there are two cards, it first tests to see if there is a pair of any other cards
    
      if(((six)==2) || ((seven)==2) || ((eight)==2) || ((nine)==2) || ((ten)==2) || ((jack)==2) || ((queen)==2) || ((king)==2)){
      System.out.println("Two Pair!"); //if there is another pair, the program prints two pair
       }
    
    else if ((two==2) || ((three)==2) || ((four)==2) || ((ace)==2)){}
    
       else  {
       System.out.println("You have a pair!"); //if no other pair is detected, the program prints that the player has a single pair
       }
break;
    
  
  case 3: //if there are three of one number-card, the program prints that there are three of that kind
    
    System.out.println("Three of a Kind!");
break;
  } //ends the switch statement    
    
    
    
    
    
switch(six){ //if there are two or three of a number-card detected, the program proceeds to determine whether it is a pair, two pair, or three of a kind
  
  case 2: //if there are two cards, it first tests to see if there is a pair of any other cards
    
      if(((seven)==2) || ((eight)==2) || ((nine)==2) || ((ten)==2) || ((jack)==2) || ((queen)==2) || ((king)==2)){
      System.out.println("Two Pair!"); //if there is another pair, the program prints two pair
       }
    
      else if ((two==2) || ((three)==2) || ((four)==2) || ((five)==2) || ((ace)==2)){ }
    
       else  {
       System.out.println("You have a pair!"); //if no other pair is detected, the program prints that the player has a single pair
       }
break;
    
  
  case 3: //if there are three of one number-card, the program prints that there are three of that kind
    
    System.out.println("Three of a Kind!");
break;
  } //ends the switch statement    
    
    
    
    
switch(seven){ //if there are two or three of a number-card detected, the program proceeds to determine whether it is a pair, two pair, or three of a kind
  
  case 2: //if there are two cards, it first tests to see if there is a pair of any other cards
    
      if(((eight)==2) || ((nine)==2) || ((ten)==2) || ((jack)==2) || ((queen)==2) || ((king)==2)){
      System.out.println("Two Pair!"); //if there is another pair, the program prints two pair
       }
    
    else if ((two==2) || ((three)==2) || ((four)==2) || ((five)==2) || ((six)==2) || ((ace)==2)){}
    
       else  {
       System.out.println("You have a pair!"); //if no other pair is detected, the program prints that the player has a single pair
       }
break;
    
  
  case 3: //if there are three of one number-card, the program prints that there are three of that kind
    
    System.out.println("Three of a Kind!");
break;
  } //ends the switch statement    
    
    
    
    
 switch(eight){ //if there are two or three of a number-card detected, the program proceeds to determine whether it is a pair, two pair, or three of a kind
  
  case 2: //if there are two cards, it first tests to see if there is a pair of any other cards
    
      if(((nine)==2) || ((ten)==2) || ((jack)==2) || ((queen)==2) || ((king)==2)){
      System.out.println("Two Pair!"); //if there is another pair, the program prints two pair
       }
    
     else if ((two==2) || ((three)==2) || ((four)==2) || ((five)==2) || ((six)==2) || ((seven)==2) || ((ace)==2)){}
     
       else  {
       System.out.println("You have a pair!"); //if no other pair is detected, the program prints that the player has a single pair
       }
break;
    
  
  case 3: //if there are three of one number-card, the program prints that there are three of that kind
    
    System.out.println("Three of a Kind!");
break;
  } //ends the switch statement   
    
    
switch(nine){ //if there are two or three of a number-card detected, the program proceeds to determine whether it is a pair, two pair, or three of a kind
  
  case 2: //if there are two cards, it first tests to see if there is a pair of any other cards
    
      if(((ten)==2) || ((jack)==2) || ((queen)==2) || ((king)==2)){
      System.out.println("Two Pair!"); //if there is another pair, the program prints two pair
       }
    
    else if ((two==2) || ((three)==2) || ((four)==2) || ((five)==2) || ((six)==2) || ((seven)==2) || ((eight)==2) || ((ace)==2)){}
    
       else  {
       System.out.println("You have a pair!"); //if no other pair is detected, the program prints that the player has a single pair
       }
break;
    
  
  case 3: //if there are three of one number-card, the program prints that there are three of that kind
    
    System.out.println("Three of a Kind!");
break;
  } //ends the switch statement    
    
    
    
switch(ten){ //if there are two or three of a number-card detected, the program proceeds to determine whether it is a pair, two pair, or three of a kind
  
  case 2: //if there are two cards, it first tests to see if there is a pair of any other cards
    
      if(((jack)==2) || ((queen)==2) || ((king)==2)){
      System.out.println("Two Pair!"); //if there is another pair, the program prints two pair
       }
    
    else if ((two==2) || ((three)==2) || ((four)==2) || ((five)==2) || ((six)==2) || ((seven)==2) || ((eight)==2) || ((nine)==2) || ((ace)==2)){}
    
       else  {
       System.out.println("You have a pair!"); //if no other pair is detected, the program prints that the player has a single pair
       }
break;
    
  
  case 3: //if there are three of one number-card, the program prints that there are three of that kind
    
    System.out.println("Three of a Kind!");
break;
  } //ends the switch statement    

    
    
switch(jack){ //if there are two or three of a number-card detected, the program proceeds to determine whether it is a pair, two pair, or three of a kind
  
  case 2: //if there are two cards, it first tests to see if there is a pair of any other cards
    
      if(((queen)==2) || ((king)==2)){
      System.out.println("Two Pair!"); //if there is another pair, the program prints two pair
       }
    
    else if ((two==2) || ((three)==2) || ((four)==2) || ((five)==2) || ((six)==2) || ((seven)==2) || ((eight)==2) || ((nine)==2) || ((ace)==2)){}
    
       else  {
       System.out.println("You have a pair!"); //if no other pair is detected, the program prints that the player has a single pair
       }
break;
    
  
  case 3: //if there are three of one number-card, the program prints that there are three of that kind
    
    System.out.println("Three of a Kind!");
break;
  } //ends the switch statement    
    
    
switch(queen){ //if there are two or three of a number-card detected, the program proceeds to determine whether it is a pair, two pair, or three of a kind
  
  case 2: //if there are two cards, it first tests to see if there is a pair of any other cards
    
      if(((king)==2)){
      System.out.println("Two Pair!"); //if there is another pair, the program prints two pair
       }
     
    else if ((two==2) || ((three)==2) || ((four)==2) || ((five)==2) || ((six)==2) || ((seven)==2) || ((eight)==2) || ((nine)==2) || ((ten)==2) || ((jack)==2) || ((ace)==2)){
       }
    
       else  {
       System.out.println("You have a pair!"); //if no other pair is detected, the program prints that the player has a single pair
       }
break;
    
  
  case 3: //if there are three of one number-card, the program prints that there are three of that kind
    
    System.out.println("Three of a Kind!");
break;
  } //ends the switch statement    
    
    
//the king switch statement is a special case. since it has already been tested for two pair against all other card types, there is no else if statement required     

    switch(king){ //if there are two or three of a number-card detected, the program proceeds to determine whether it is a pair, two pair, or three of a kind
  
  case 2: //if there are two cards, it first tests to see if there is a pair of any other cards
    
      if ((two==2) || ((three)==2) || ((four)==2) || ((five)==2) || ((six)==2) || ((seven)==2) || ((eight)==2) || ((nine)==2) || ((ten)==2) || ((jack)==2) || ((ace)==2) || ((queen)==2)){}
       
        else {
          System.out.println("You have a pair!"); //if no other pair is detected, the program prints that the player has a single pair
        }
break;
    
  
  case 3: //if there are three of one number-card, the program prints that there are three of that kind
    
    System.out.println("Three of a Kind!");
break;
  } //ends the switch statement  
    
if (((ace)<= 1) && ((two)<= 1) && ((three)<= 1) && ((four)<=1) && ((five)<=1) && ((six)<=1) && ((seven)<=1) && ((eight)<=1) && ((nine)<=1) && ((ten)<=1) && (jack<=1) && ((queen)<=1) && ((king)<=1)){
  System.out.println("You Have A High Card Hand.");
}
        
      
  } //end of main method
} //end of class 