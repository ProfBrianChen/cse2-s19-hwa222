//Henry Anderson HW07 for CSE2 on 3/8/19
//The following program calculatestes areas

//imports scanner class
import java.util.Scanner;
//creates essential class for the start of every program
public class Area {            
  //creates essential main method
  public static void main(String[] args) {  
     //creates new scanner "myscanner"  
  Scanner myScanner = new Scanner( System.in );
  
  System.out.print("What type of shape?");
    
    String shape = myScanner.next();
    double area = 0;
    if (shape.equals("rectangle")){
      System.out.print("Enter the length");
      //call check
     double length = check();
     System.out.print("Enter the width");
     double width = check();
     //Calculate area
     area = rectangle(length, width);
    }
    else if(shape.equals("triangle")){
      System.out.print("Enter the height");
      //call check
     double height = check();
     System.out.print("Enter the base");
     double base = check();
     //Calculate area
     area = triangle(height, base);
    }
    else if (shape.equals("circle")){
      System.out.print("Enter the radius");
      //call check
     double radius = check();
     //Calculate area
     area = circle(radius);
  }
    else{
      System.out.print("You did not enter a valid shape. please enter rectangle, triangle, or circle.");
    }
    System.out.print("The area is " + area);
  }
    
  public static double rectangle(double length, double width){
    return (length*width);
  }
  public static double triangle(double height, double base){
    return(height*base*(.5));
  }
  public static double circle(double radius){
    return(radius*radius*(3.14));
  }
  public static double check(){
    double value;
    Scanner checkScanner = new Scanner( System.in );
    while ( checkScanner.hasNextDouble() == false ) {
      System.out.print("Value Needs to be a Double");
    }
    value = checkScanner.nextDouble();
    return value;
  }
  
  
    
  }