//Henry Anderson Lab08 for CSE2 on 4/5/19
//The following program utilizes arrays

//imports classes
import java.util.Arrays;
import java.util.Random;
import java.lang.Math; 
//creates essential class for the start of every program
public class lab08 {            
  //creates essential main method
  public static void main(String[] args) {
   
   Random randomGenerator = new Random();
   int randomInt = randomGenerator.nextInt(50);
   int number=(randomInt+50);
   System.out.print(number);
   int[] numbers = new int[number];
    
   
    for(int i=numbers.length-1;i>=0;i--) {
      numbers[i] = randomGenerator.nextInt(100);
      //System.out.println(numbers[i]);
    }
    
    int range= getRange(numbers, number);
    int mean=  getMean(numbers, number);
    int stddev= getStdDev(numbers, number);
    System.out.println("The range is"+range);
    System.out.println("The mean is" + mean);
    System.out.println("The Standard Deviation is" + stddev);
    shuffle(numbers);
     for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
  }
  }
    public static int getRange(int [] numbers, int number) {   
      Arrays.sort(numbers);
      int min = numbers[0];
      int max = numbers[number-1];
      int range =(max-min);
      return range;
    }
    public static int getMean(int [] numbers, int number) {   
      int counter=0;
      for(int i = 0; i < numbers.length; i++){
        counter= numbers[i] + counter;
      }
      int mean = ((counter)/number);
      return mean;
    }
     public static int getStdDev(int [] numbers, int number) {   
      int counter=0;
       int stddev = 0;
      for(int i = 0; i < numbers.length; i++){
        counter= numbers[i] + counter;
      int mean = ((counter)/number);
      stddev= (int) Math.sqrt(((counter)-(mean)*2/((number)-1)));
       
      } return stddev;
     }
      public static void shuffle(int[] n) {
        Random random = new Random();
        for (int i = 0; i < n.length; i++) {
            int indexOne = random.nextInt(n.length);
            int indexTwo = random.nextInt(n.length);
            int temp = n[indexOne];
            n[indexOne] = n[indexTwo];
            n[indexTwo] = temp;
        }

    }
}
