//Henry Anderson Lab 04 for CSE2 on 2/15/2019
//The following program will simulate drawing a "random" card from a deck


public class CardGenerator {            //creates essential class for the start of every program
  
  public static void main(String[] args) {             //creates essential main method for the start of every program
        
    
int rand = (int)(Math.random() * ((52) + 1)); //generates random number between 1-52, representing the number of cards in the deck
    
    //System.out.println(rand); used to test if random number was generated correctly

int suit = (rand%4); //uses modulus to calculate for suit
    
    //System.out.println(suit); used to test if suit was generated correctly
    
int num = (rand%13); //uses modulus command to calculate for number card (ace, king, 3, etc)
    
    //System.out.println(num); used to test if num wss generated correctly
    
if (num==0) {
  System.out.print("You got an Ace of");   //prints number value(Ace, King 3, etc.)
} 
    
else if (num==1) {
  System.out.print("You got a 2 of");   //prints number value(Ace, King 3, etc.)
} 

else if (num==2) {
  System.out.print("You got a 3 of");   //prints number value(Ace, King 3, etc.)
} 
   
else if (num==3) {
  System.out.print("You got a 4 of");   //prints number value(Ace, King 3, etc.)
} 
    
else if (num==4) {
  System.out.print("You got a 5 of");   //prints number value(Ace, King 3, etc.)
} 
    
else if (num==5) {
  System.out.print("You got a 6 of");   //prints number value(Ace, King 3, etc.)
} 
    
else if (num==6) {
  System.out.print("You got a 7 of");   //prints number value(Ace, King 3, etc.)
} 

else if (num==7) {
  System.out.print("You got a 8 of");   //prints number value(Ace, King 3, etc.)
} 
    
else if (num==8) {
  System.out.print("You got a 9 of");   //prints number value(Ace, King 3, etc.)
} 
    
else if (num==9) {
  System.out.print("You got a 10 of");   //prints number value(Ace, King 3, etc.)
} 
    
else if (num==10) {
  System.out.print("You got a Jack of");   //prints number value(Ace, King 3, etc.)
} 
    
else if (num==11) {
  System.out.print("You got a Queen of");   //prints number value(Ace, King 3, etc.)
} 
    
else if (num==12) {
  System.out.print("You got a King of");   //prints number value(Ace, King 3, etc.)
}     
    
if (suit == 0) {
  System.out.print(" Spades! ") ;   //printing out suit(spades,hearts,kings,clubs)
}

else if (suit == 1) {
  System.out.print(" Hearts! ");   //printing out suit(spades,hearts,kings,clubs)
}
    
else if (suit == 2) {
  System.out.print(" Clubs! ");   //printing out suit(spades,hearts,kings,clubs)
}
    
else if (suit == 3) {
  System.out.print(" Diamonds! ");   //printing out suit(spades,hearts,kings,clubs)
}
    
System.out.println(""); //blank for spacing
   

  
  
  
  
  
  }
}