//Henry Anderson Lab06 for CSE2 on 3/8/19
//The following program takes an input value for length from 1 to 10 and prints out a predetermined pattern

//imports scanner class
import java.util.Scanner;
//creates essential class for the start of every program
public class PatternB {            
  //creates essential main method
  public static void main(String[] args) {             
  //creates new scanner "myscanner"  
  Scanner myScanner = new Scanner( System.in ); 
        
   
    //prints statement prompting user to enter value of the length    
  System.out.print("Enter the length: ");      
 //prints statement prompting user to re-enter valid value of the length if an integer is not entered. dumps entered string into unused variable junkWord
  while ( myScanner.hasNextInt() == false ) {
    System.out.print("Length Needs To Be An Integer! ");      
    String junkWord = myScanner.next();    
  
  }
//prints statement prompting user to re-enter valid value of the length if integer is not between 1 and 10. dumps entered negative int into unused variable junkInt    
    int length = myScanner.nextInt();
    while (length <=0 | length >10) {  
      System.out.print("Length Needs To Be Between 1 and 10! ");        
      int junkInt = myScanner.nextInt();    
  }    
//assigns length to n, variable for number of rows    
  int n= length;
//utilizes nested for loop to print pattern
  for (int i = n; i>0; i--) {
    for (int j = 1; j <= i; j++) 
      System.out.print(j);
      System.out.println();
}
    
    
    
//end of main method    
  }
//end of class  
}