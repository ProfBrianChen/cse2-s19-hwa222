//Henry Anderson Lab06 for CSE2 on 3/8/19
//The following program takes an input value for length from 1 to 10 and prints out a predetermined pattern

//imports scanner class
import java.util.Scanner;
//creates essential class for the start of every program
public class PatternC {            
  //creates essential main method
  public static void main(String[] args) {             
  //creates new scanner "myscanner"  
  Scanner myScanner = new Scanner( System.in ); 
        
   
    //prints statement prompting user to enter value of the length    
  System.out.print("Enter the length: ");      
 //prints statement prompting user to re-enter valid value of the length if an integer is not entered. dumps entered string into unused variable junkWord
  while ( myScanner.hasNextInt() == false ) {
    System.out.print("Length Needs To Be An Integer! ");      
    String junkWord = myScanner.next();    
  
  }
//prints statement prompting user to re-enter valid value of the length if integer is not between 1 and 10. dumps entered negative int into unused variable junkInt    
    int length = myScanner.nextInt();
    while (length <=0 | length >10) {  
      System.out.print("Length Needs To Be Between 1 and 10! ");        
      int junkInt = myScanner.nextInt();    
  }    
//assigns length to n, variable for number of rows    
  int n= length;
//utilizes nested for loop to print pattern
  for (int i = 1; i<=n; i++) {
    for (int j = i; j>0; j--) 
      if(j!=6)
        System.out.print("");
      if(j!=5){
        System.out.print("");}
      if(j!=4){
        System.out.print("");}
      if(j!=3){
        System.out.print("");}
       if (j!=2){
          System.out.print("");}
    else{
    System.out.print(j);}
          
      System.out.println();
}
    
    
    
//end of main method    
  }
//end of class  
}
