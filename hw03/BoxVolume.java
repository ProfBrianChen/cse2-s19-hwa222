//Henry Anderson HW #3 for CSE2 on 2/11/2019
//The following program will take inputs from the user using the scanner class and determine the volume of the box

import java.util.Scanner; //imports scanner class, must happen before declaring class

public class BoxVolume {            //creates essential class for the start of every program
  
  public static void main(String[] args) {             //creates essential main method for the start of every program
    
    
    Scanner myScanner = new Scanner( System.in );        //declares instance of the Scanner object, calls the Scanner constructor
    
    System.out.print("Enter the length of the box: ");      //prints statement prompting user to enter value of the length
    
    double length = myScanner.nextDouble();  //takes next double input and stores it as "length" input can also be an int because of implicit casting
    
    System.out.print("Enter the width of the box: ");      //prints statement prompting user to enter value of the width
    
    double width = myScanner.nextDouble();  //takes next double input and stores it as "width" input can also be an int because of implicit casting
    
    System.out.print("Enter the height of the box: ");      //prints statement prompting user to enter value of the height
    
    double height = myScanner.nextDouble();  //takes next double input and stores it as "height" input can also be an int because of implicit casting
    
    double volume = (length * width * height); //calculates volume using formula for box (length times width times height)
      
    System.out.println("The length of the box is: "+(length)+""); //prints the length
    
    System.out.println("The width side of the box is: "+(width)+""); //prints the width
    
    System.out.println("The height of the box is: "+(height)+"");  //prints the height
    
    System.out.println("The volume inside the box is: "+(volume)+""); //prints the total volume
    
  }//end of the main method
}//end of class