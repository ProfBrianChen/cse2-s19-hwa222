//Henry Anderson HW #3 for CSE2 on 2/11/19
//The following program will prompt the user for a measurement in meters, and print the same measurement converted into inches


import java.util.Scanner; //imports scanner class, must happen before declaring class

public class Convert {            //creates essential class for the start of every program
  
  public static void main(String[] args) {             //main method required for every java program
    
    Scanner myScanner = new Scanner( System.in );        //declares instance of the Scanner object, calls the Scanner constructor
    
    System.out.print("Enter the distance in meters: ");      //prints statement prompting user to enter value of distance (in meters)
    
    double distancemeter = myScanner.nextDouble();  //takes next double input and stores it as "distancemeter" input can also be an int because of implicit casting
    
    double distanceinches = (double) (distancemeter*39.37); //takes value for meters given and converts it to inches (multiplies it by 39.37)
    
    System.out.println(" "+(distancemeter)+" meters is "+(distanceinches)+" inches.");
    
  } //end of main method
}// end of class
    