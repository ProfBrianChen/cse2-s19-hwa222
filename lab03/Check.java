//Henry Anderson Lab 03 for CSE2 on 2/8/2019
//The following program will take an input from the user using the scanner class and determine the total dinner cost, including tip

//imports scanner class, must happen before declaring class
import java.util.Scanner;

//creates essential class for the start of every program
public class Check {
  //main method required for every Java program
  public static void main(String[] args) {
    
    //declares instance of the Scanner object, calls the Scanner constructor
    Scanner myScanner = new Scanner( System.in );
    
    //prints statement prompting user to enter value of meal cost (without tip)
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    
    //takes next double input and stores it as "checkCost" input can also be an int because of implicit casting
    double checkCost = myScanner.nextDouble();
    
    //prints statement prompting user to enter the desired tip percentage
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );
    
    //takes next double input and stores it as "tipPercent"
    double tipPercent = myScanner.nextDouble();
    
    //divides tip percent value by 100 to convert it to a decimal, so it can be easily used in multiplication
    tipPercent /= 100; //We want to convert the percentage into a decimal value

    //prints statement prompting user to enter the number of people who went to dinner
    System.out.print("Enter the number of people who went out to dinner: ");
    
    //takes next integer input and assings it to "numPeople"
    int numPeople = myScanner.nextInt();
    
    //creates variables TotalCost and costPerPerson
    double totalCost;
    double costPerPerson;
    
    //creates variables for whole dollar amount, as well as dimes and pennies for storing digits to the right of the decimal point
    int dollars;    
    int dimes; 
    int pennies; 
          
totalCost = checkCost * (1 + tipPercent); //calculates total cost with tip
    
costPerPerson = totalCost / numPeople; //divides total cost by number of people
    
//get the whole amount, dropping decimal fraction
dollars=(int)costPerPerson;
    
//get dimes amount, e.g., 
// (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
//  where the % (mod) operator returns the remainder
//  after the division:   583%100 -> 83, 27%5 -> 2 
dimes=(int)(costPerPerson * 10) % 10;
pennies=(int)(costPerPerson * 100) % 10; 
System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); //prints final cost per person
      
  }
}