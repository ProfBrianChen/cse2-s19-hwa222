//Henry Anderson HW 05 for CSE2 on 3/4/2019
//The following program will take inputs for class information and ensure that all information is of the proper typing

//imports scanner class
import java.util.Scanner;
//creates essential class for the start of every program
public class Hw05 {            
  //creates essential main method
  public static void main(String[] args) {             
  //creates new scanner "myscanner"  
  Scanner myScanner = new Scanner( System.in ); 
  //declare variables for printing twist later on in code
  int coursenumber, timesperweek, numberofstudents; 
  double starttime;
  String departmentname="";
  String instructorname="";
    
    //prints statement prompting user to enter value    
  System.out.print("Please Enter the Course Number: ");      
  //prints statement prompting user to re-enter valid value for course number if an integer is not entered. dumps entered string into unused variable junkWord
  while ( myScanner.hasNextInt() == false ) {    
    System.out.print("Course Number needs to be an Integer! ");       
          String junkWord = myScanner.next();     
  }
    //if number is entered correctly, value is stored
    coursenumber = myScanner.nextInt();
    
    
    //prints statement prompting user to enter department name    
  System.out.print("Please Enter the Department Name: ");      
  //prints statement prompting user to re-enter valid value for department name if a string is not entered. dumps entered string into unused variable junkWord
  while ( myScanner.hasNextInt() == true ) {    
    System.out.print("Department Name needs to be a String! (Can't Be a Number) ");       
          String junkWordstringa = myScanner.next();     
  }
   while ( myScanner.hasNextDouble() == true ) {    
    System.out.print("Department Name needs to be a String! (Can't Be a Number) ");       
          String junkWordstringb = myScanner.next();     
  } 
    //if string is entered correctly, value is stored
    departmentname = departmentname + myScanner.next();
     
   
    //prints statement prompting user to enter value    
  System.out.print("Please Enter the Number of Times The Class Meets Per Week: ");      
  //prints statement prompting user to re-enter valid value for number of meetings. if an integer is not entered. dumps entered string into unused variable
  while ( myScanner.hasNextInt() == false ) {    
    System.out.print("Amount of Meetings Needs to be an Integer! ");       
          String junkWord2 = myScanner.next();     
  }
    //if number is entered correctly, value is stored
    timesperweek = myScanner.nextInt();
    
       
    //prints statement prompting user to enter start time    
  System.out.print("Please Enter the Time the Class Starts: ");      
  //prints statement prompting user to re-enter valid value for course number if an integer is not entered. dumps entered string into unused variable junkWord
  while ( myScanner.hasNextDouble() == false ) {    
    System.out.print("Start Time needs to be a Double! (HH MM format)");       
          String junkWorddouble = myScanner.next();     
  }
    //if number is entered correctly, value is stored
    starttime = myScanner.nextDouble();
    
    
    //prints statement prompting user to enter department name    
  System.out.print("Please Enter the Instructor Name: ");      
  //prints statement prompting user to re-enter valid value for instructor name if a string is not entered. dumps entered string into unused variable junkWord
  while ( myScanner.hasNextInt() == true ) {    
    System.out.print("Instructor Name needs to be a String! (Can't Be a Number) ");       
          String junkWordstring1 = myScanner.next();     
  }
    while ( myScanner.hasNextDouble() == true ) {    
    System.out.print("Instructor Name needs to be a String! (Can't Be a Number) ");       
          String junkWordstring2 = myScanner.next();     
  }
    //if string is entered correctly, value is stored
    instructorname = instructorname + myScanner.next();
    
    
    //prints statement prompting user to enter value    
  System.out.print("Please Enter the Number of Students in the Class: ");      
  //prints statement prompting user to re-enter valid value for number of students. if an integer is not entered. dumps entered string into unused variable
  while ( myScanner.hasNextInt() == false ) {    
    System.out.print("Number of Students Needs to be an Integer! ");       
          String junkWord3 = myScanner.next();     
  }
    //if number is entered correctly, value is stored
    numberofstudents = myScanner.nextInt();
    
    //prints out entered components
    System.out.println("Course Number is " + coursenumber);
    System.out.println("Department Name is " +departmentname);
    System.out.println("Class Meets " + timesperweek + " Times per Week");
    System.out.println("Start Time is " + starttime);
    System.out.println("Instructor Name is " + instructorname);
    System.out.println("Number of students is " + numberofstudents);
   }
  }
  