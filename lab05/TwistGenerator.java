//Henry Anderson Lab 05 for CSE2 on 3/1/2019
//The following program will take in integer input for length and print out a twist that corresponds to the value

//imports scanner class
import java.util.Scanner;
//creates essential class for the start of every program
public class TwistGenerator {            
  //creates essential main method
  public static void main(String[] args) {             
  //creates new scanner "myscanner"  
  Scanner myScanner = new Scanner( System.in ); 
  //declare variables for printing twist later on in code
  int i; 
  String top="";
  String middle="";
  String bottom="";
    
//prints statement prompting user to enter value of the length    
System.out.print("Enter the length: ");      
 //prints statement prompting user to re-enter valid value of the length if an integer is not entered. dumps entered string into unused variable junkWord
  while ( myScanner.hasNextInt() == false ) {
    
    System.out.print("Length Needs To Be A Postive Integer! ");      
    
          String junkWord = myScanner.next();    
  
  }
//prints statement prompting user to re-enter valid value of the length if integer is not postive. dumps entered negative int into unused variable junkInt    
int value = myScanner.nextInt();
  while (value<=0) {  
    System.out.print("Length Needs To Be A Postive Integer! ");        
          int junkInt = myScanner.nextInt();    
  }
 //assigns length to value variable   
    int length = value;  
    //adds correct number of vertical rows corresponding to length using given pattern. Assigns characters to previously declared strings
    for (i=1; i<=(length); i++) {
      if (i%3==1) {
          top= top + "\\";
          middle= middle + " ";
          bottom= bottom + "/";
      }
      if (i%3==2) {
          top= top + " ";
          middle= middle + "X";
          bottom= bottom + " ";
      }
      if (i%3==0) {
          top= top + "/";
          middle= middle + " ";
          bottom= bottom + "\\";
      }
    }
    //prints out completed strings
      System.out.println(top);
      System.out.println(middle);
      System.out.println(bottom);
      
    
    
    
    
    
  } //end of main method 
}//end of class
    