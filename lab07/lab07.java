//Henry Anderson Lab07 for CSE2 on 3/29/19
//The following program takes an input value for length from 1 to 10 and prints out a predetermined pattern

//imports scanner class
import java.util.Random;
//creates essential class for the start of every program
public class lab07 {            
  //creates essential main method
  public static void main(String[] args) {  
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(5);
    //System.out.print(randomInt);
    String adj = adjectives(randomInt);
    String sub = subjects(randomInt);
    String verb = verbs(randomInt);
    String obj = objects(randomInt);
    System.out.println("The "+adj+" "+sub+" "+verb+" the "+obj);
  }
    public static String adjectives(int randomInt) {
      String adj="";
      switch(randomInt) {
        case 0:
          adj= "fast";
        break;
          case 1:
          adj= "charming";
        break;
          case 2:
          adj="sneaky";
        break;
          case 3:
          adj= "pretty";
        break;
          case 4:
          adj = "strong";
        break;
      } return adj;
    }
    public static String subjects(int randomInt){
      String sub="";
      switch(randomInt) {
        case 0:
          sub="dog";
        break;
          case 1:
          sub="cat";
        break;
          case 2:
          sub= "fox";
        break;
          case 3:
          sub= "cub";
        break;
          case 4:
          sub= "fawn";
        break;     
      } return sub;
    }
     public static String verbs(int randomInt) {
       String verb="";
       switch(randomInt) {
        case 0:
          verb="ran past";
        break;
          case 1:
          verb="jumped over";
        break;
          case 2:
          verb= "glided by";
        break;
          case 3:
          verb= "cut through";
        break;
          case 4:
          verb= "cleared";
        break;         
      } return verb;
     }
     public static String objects(int randomInt){
      String obj="";
       switch(randomInt) {
        case 0:
          obj= "fence";
        break;
          case 1:
          obj= "wall";
        break;
          case 2:
          obj= "ditch";
        break;
          case 3:
          obj= "stream";
        break;
          case 4:
          obj= "gulley";
        break;          
      } return obj;
     }
}