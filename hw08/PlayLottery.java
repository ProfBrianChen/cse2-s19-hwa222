import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;

public class PlayLottery {

    public static void main(String [] args)
    {

      Scanner myScanner = new Scanner(System.in);
      int[] User = new int[5];

      System.out.print("Enter five numbers between 0 and 59");
      for (int i=0; i<5; i++) 
      {
        System.out.print(":");
        User[i]=myScanner.nextInt();
      }
      System.out.println();

      Random randomGenerator = new Random();

      int[] Random = new int[5];

      for (int i=0; i<5; i++) 
      {
        Random[i]= randomGenerator.nextInt(59);
      }

      int count = 0;

      for (int i=0; i<5; i++) 
      {
        if (User[i]==Random[i])
        {
          count++; 
        }
      }
      System.out.print("The winning numbers are:");

     for(int i=0; i<5; i++) 
     {
       System.out.print(Random[i] + ", ");

     }
      System.out.println(""); 

      if (count==5) {
      System.out.println("Congrats You Won");
      }
      else {
        System.out.println("You Lose");
      }
    }
}